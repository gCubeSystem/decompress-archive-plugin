This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for org.gcube.data.transfer.decompress-archive-plugin

## [v1.0.2] 2020-09-07

### Fixes

- Integration with gcube distribution (https://support.d4science.org/issues/19612)
